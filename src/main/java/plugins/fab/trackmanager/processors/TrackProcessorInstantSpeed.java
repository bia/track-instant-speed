package plugins.fab.trackmanager.processors;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.apache.poi.ss.usermodel.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import icy.canvas.IcyCanvas;
import icy.gui.dialog.MessageDialog;
import icy.gui.util.ComponentUtil;
import icy.gui.util.GuiUtil;
import icy.main.Icy;
import icy.math.MathUtil;
import icy.painter.Painter;
import icy.sequence.Sequence;
import plugins.adufour.workbooks.Workbooks;
import plugins.adufour.workbooks.Workbooks.WorkbookFormat;
import plugins.fab.trackmanager.PluginTrackManagerProcessor;
import plugins.fab.trackmanager.TrackSegment;
import plugins.nchenouard.spot.Detection;

/**
 * @author Fabrice de Chaumont
 */
public class TrackProcessorInstantSpeed extends PluginTrackManagerProcessor implements Painter, ActionListener
{

    public TrackProcessorInstantSpeed()
    {

        setName("Instant Speed Track Processor");

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.add(chartpanel);
        affectColorButton.setSelected(false);
        affectColorButton.addActionListener(this);

        panel.add(GuiUtil.createLineBoxPanel(affectColorButton, displayTextCheckBox, displayLegendCheckBox));
        panel.add(GuiUtil.createLineBoxPanel(displayGraphInSequenceCheckBox, useRoiAsBoundsForChartButton,
                forceAllSequenceGraphWidthCheckBox));
        ComponentUtil.setFixedWidth(scaleTextField, 100);
        panel.add(GuiUtil.createLineBoxPanel(new JLabel("Scale:"), scaleTextField));
        panel.add(GuiUtil.createLineBoxPanel(exportVelocitiesXLSButton));

        scaleTextField.addActionListener(this);
        useRoiAsBoundsForChartButton.addActionListener(this);
        displayGraphInSequenceCheckBox.addActionListener(this);
        displayTextCheckBox.addActionListener(this);
        displayLegendCheckBox.addActionListener(this);
        forceAllSequenceGraphWidthCheckBox.addActionListener(this);
        // useRealScalesBox.addActionListener(this);
        exportVelocitiesTxtFileButton.addActionListener(this);
        exportVelocitiesXLSButton.addActionListener(this);
        // trackPool.getDisplaySequence().addSequenceChangeListener( this );
        df = new DecimalFormat("#########.00");
        // trackPool.getSequence().addPainter( this );

    }

    DecimalFormat df;
    JFreeChart chart;
    JPanel chartpanel = new JPanel();
    JCheckBox affectColorButton = new JCheckBox("Affect colors.");
    ArrayList<Painter> painterList = new ArrayList<Painter>();
    JCheckBox displayTextCheckBox = new JCheckBox("Display text.", false);
    JCheckBox displayLegendCheckBox = new JCheckBox("Display legend.", false);
    JCheckBox displayGraphInSequenceCheckBox = new JCheckBox("Display graph on sequence.", false);
    JButton useRoiAsBoundsForChartButton = new JButton("place graph in ROI #1");
    JCheckBox forceAllSequenceGraphWidthCheckBox = new JCheckBox("Force graph width.", false);
    // JCheckBox useRealScalesBox = new JCheckBox("Use real scales", false);
    JTextField scaleTextField = new JTextField("1.0");
    JButton exportVelocitiesTxtFileButton = new JButton("export to text file");
    JButton exportVelocitiesXLSButton = new JButton("export to excel");
    boolean useRealScale = true; // useRealScalesBox.isSelected();

    @Override
    public void Compute()
    {
        if (!isEnabled())
            return;
        painterList.clear();

        Sequence sequence = trackPool.getDisplaySequence();
        if (sequence != null)
        {
            if (!sequence.contains(this))
                sequence.addPainter(this);
        }

        /** Max distance between 2 consecutives detections */
        double maxDistance = 0;

        if (affectColorButton.isSelected()) // The max distance is used to normalize the coloration of the track
        {
            if (useRealScale)
            {
                for (TrackSegment ts : trackPool.getTrackSegmentList())
                {
                    ArrayList<Detection> detectionList = ts.getDetectionList();
                    for (int i = 0; i < detectionList.size() - 1; i++)
                    {
                        Detection d1 = detectionList.get(i);
                        Detection d2 = detectionList.get(i + 1);
                        if (d1.isEnabled() && d2.isEnabled())
                        {
                            double dist = getScaledDistance(d1, d2);
                            if (dist > maxDistance)
                                maxDistance = dist;
                        }
                    }
                }
            }
            else
            {
                for (TrackSegment ts : trackPool.getTrackSegmentList())
                {
                    ArrayList<Detection> detectionList = ts.getDetectionList();
                    for (int i = 0; i < detectionList.size() - 1; i++)
                    {
                        Detection d1 = detectionList.get(i);
                        Detection d2 = detectionList.get(i + 1);
                        if (d1.isEnabled() && d2.isEnabled())
                        {
                            double dist = getDistance(d1, d2);
                            if (dist > maxDistance)
                                maxDistance = dist;
                        }
                    }
                }
            }
        }

        Sequence displaySequence = trackPool.getDisplaySequence();
        if (displaySequence == null)
            return;
        double timeInterval = displaySequence.getTimeInterval();
        // display colors and generates graph
        float ratio = 1;
        if (maxDistance > 0)
        {
            if (useRealScale)
                ratio = (float) (1f / (maxDistance / timeInterval));
            // ratio = (float) (1f / maxDistance);
            else
                ratio = (float) (1f / maxDistance);
        }

        XYSeriesCollection xyDataset = new XYSeriesCollection();
        for (TrackSegment ts : trackPool.getTrackSegmentList())
        // if ( ts.isAllDetectionEnabled() )
        {
            XYSeries series = new XYSeries("Track " + trackPool.getTrackIndex(ts));

            ArrayList<Detection> detectionList = ts.getDetectionList();
            for (int i = 0; i < detectionList.size() - 1; i++)
            {
                Detection d1 = detectionList.get(i);
                Detection d2 = detectionList.get(i + 1);

                if (d1.isEnabled() && d2.isEnabled())
                {

                    double velocity;
                    double t;
                    if (useRealScale)
                    {
                        double dist = getScaledDistance(d1, d2);
                        velocity = (dist / timeInterval);
                        t = d1.getT() * timeInterval;
                        // velocity = (float) (dist/super.trackPool.getSequence().getTScale());
                        // PluginConsole.println("scales "+super.trackPool.getSequence().getXScale()+" "+super.trackPool.getSequence().getYScale()+"
                        // "+super.trackPool.getSequence().getZScale());
                        // PluginConsole.println("xdist "+(d1.getX()-d2.getX()));
                        // PluginConsole.println("xdist "+super.trackPool.getSequence().getXScale()*(d1.getX()-d2.getX())*10e6);
                        // PluginConsole.println("ydist "+(d1.getY()-d2.getY()));
                        // PluginConsole.println("ydist "+super.trackPool.getSequence().getYScale()*10e6*(d1.getY()-d2.getY()));
                        // PluginConsole.println("zdist "+super.trackPool.getSequence().getZScale()*(d1.getZ()-d2.getZ())*10e6);
                        // PluginConsole.println("zdist "+(d1.getZ()-d2.getZ()));
                        // PluginConsole.println("dist "+dist+" velocity "+velocity+" tscale "+super.trackPool.getSequence().getTScale());
                        // t = (float)(((float)d1.getT())*super.trackPool.getSequence().getTScale());
                    }
                    else
                    {
                        velocity = getDistance(d1, d2);
                        t = d1.getT();
                    }
                    series.add(t, velocity);

                    if (displayTextCheckBox.isSelected())
                    {
                        // TODO: re-enable this
                        // if ( (int)d2.getT() == trackPool.getSequence().getSelectedT() )
                        // painterList.add( new TextPainter( trackPool.getSequence() ,
                        // (int)d2.getT(),
                        // 0,
                        // (int)d2.getX(),
                        // (int)d2.getY(),
                        // ""+df.format( velocity ) ) );
                    }

                    if (affectColorButton.isSelected())
                    {
                        float c = (float) (ratio * velocity);

                        if (c < 0 || c > 1)
                        {
                            System.out.println("Error: color needed is : " + c);
                        }
                        if (c < 0)
                            c = 0;
                        if (c > 1)
                            c = 1;

                        d1.setColor(new Color(c, c, c));
                        d2.setColor(new Color(c, c, c));
                    }
                }

            }
            xyDataset.addSeries(series);

            // chartDataArray[6].chart.draw( g2 , new Rectangle2D.Float( 250 , 20 + 260 * 0 ,490,240 ) );

            // System.out.println("compute");
            // trackPool.getSequence().addPainter( )

        }
        if (forceAllSequenceGraphWidthCheckBox.isSelected())
        {
            XYSeries series = new XYSeries("");
            series.add(trackPool.getDisplaySequence().getSizeT(), 0);
            xyDataset.addSeries(series);
        }

        // outLabel.setText( "Maximum distance in 1 t = " + maxDistance + "px." );

        String TitleString = "";
        String TitleString2 = "";
        String TitleString3 = "";

        if (displayLegendCheckBox.isSelected())
        {
            TitleString = "Instant Speed of tracks";
            if (useRealScale)
                TitleString2 = "Time (s)";
            else
                TitleString2 = "Time";
            if (useRealScale)
                TitleString3 = "Instant Speed (\u00B5m)/s";
            else
                TitleString3 = "Instant Speed in pixels";
        }

        chart = ChartFactory.createXYLineChart(TitleString, TitleString2, TitleString3, xyDataset,
                PlotOrientation.VERTICAL, displayLegendCheckBox.isSelected(), true, false);

        chartpanel.removeAll();

        if (chart != null)
        {
            // replace default chart colors by detection colors (taken from t=0)
            XYItemRenderer renderer = ((XYPlot) chart.getPlot()).getRenderer();
            for (TrackSegment ts : trackPool.getTrackSegmentList())
                renderer.setSeriesPaint(trackPool.getTrackIndex(ts), ts.getFirstDetection().getColor());

            // chartPanel.add(new ChartPanel(chart, 500, 300, 500, 300, 500, 300, false, false, true, true, true, true));
        }

        chartpanel.add(new ChartPanel(chart, 400, 200, 400, 200, 400, 200, false, false, true, true, true, true));
        panel.updateUI();

        /*
         * 
         */

    }

    // JLabel outLabel = new JLabel();

    public double getDistance(double x1, double y1, double z1, double x2, double y2, double z2)
    {
        double distance = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2));
        return distance;
    }

    public double getDistance(Detection d1, Detection d2)
    {
        return getDistance(d1.getX(), d1.getY(), d1.getZ(), d2.getX(), d2.getY(), d2.getZ());
    }

    public double getScaledDistance(Detection d1, Detection d2)
    {
        // return getDistance(d1, d2);
        return Math.sqrt(Math.pow(super.trackPool.getDisplaySequence().getPixelSizeX() * (d1.getX() - d2.getX()), 2)
                + Math.pow(super.trackPool.getDisplaySequence().getPixelSizeY() * (d1.getY() - d2.getY()), 2)
                + Math.pow(super.trackPool.getDisplaySequence().getPixelSizeZ() * (d1.getZ() - d2.getZ()), 2));
    }

    public void actionPerformed(ActionEvent e)
    {

        // if ( e.getSource() == affectColorButton || e.getSource() == displayTextCheckBox
        // || e.getSource() == displayLegendCheckBox )
        // {
        // trackPool.fireTrackEditorProcessorChange();
        // }

        if (e.getSource() == useRoiAsBoundsForChartButton)
        {
            try
            {
                Shape shape = (Shape) trackPool.getDisplaySequence().getROIs().get(0);
                chartRectangleInSequence = (Rectangle2D) shape.getBounds2D().clone();
                displayGraphInSequenceCheckBox.setSelected(true);
            }
            catch (Exception e2)
            {
                MessageDialog.showDialog("No Sequence or No ROI to perform the operation.",
                        MessageDialog.ERROR_MESSAGE);
            }
        }

        // if (e.getSource() == exportVelocitiesTxtFileButton)
        // {
        // exportVelocitiesToConsole();
        // analyseTracksToTextFile((ArrayList<TrackSegment>) trackPool.getTrackSegmentList().clone());
        // }

        if (e.getSource() == exportVelocitiesXLSButton)
        {
            exportToXLS((ArrayList<TrackSegment>) trackPool.getTrackSegmentList().clone());
        }

        trackPool.fireTrackEditorProcessorChange();
    }

    private void exportToXLS(ArrayList<TrackSegment> trackSegmentList)
    {
        Sequence displaySequence = trackPool.getDisplaySequence();
        if (displaySequence == null)
        {
            System.err.println("No display sequence. Export cancelled.");
            return;
        }

        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select XLSX file.");

        int returnVal = chooser.showOpenDialog(null);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;

        File file = chooser.getSelectedFile();

        final Workbook wb = Workbooks.createEmptyWorkbook(WorkbookFormat.XLSX);
        final Sheet sh = wb.createSheet("Results");

        // build header
        final Row header = sh.createRow(0);
        sh.createRow(1);
        final Row header2 = sh.createRow(2);

        header.createCell(0, CellType.STRING).setCellValue("y axis: track number");
        header.createCell(1, CellType.STRING).setCellValue("x axis: time (in second)");
        header.createCell(2, CellType.STRING).setCellValue("values in micrometer per second");
        header2.createCell(0, CellType.STRING).setCellValue("track# \\ time (s)");

        final double timeInterval = displaySequence.getTimeInterval();

        int maxT = 0;
        int trackNumber = 0;
        for (TrackSegment ts : trackPool.getTrackSegmentList())
        {
            final Row row = sh.createRow(trackNumber + 3);

            // set track index (String format)
            row.createCell(0, CellType.STRING).setCellValue(Integer.toString(trackNumber));

            final List<Detection> detectionList = ts.getDetectionList();
            for (int i = 0; i < detectionList.size() - 1; i++)
            {
                Detection d1 = detectionList.get(i);
                Detection d2 = detectionList.get(i + 1);

                if (d1.isEnabled() && d2.isEnabled())
                {
                    final int t = d1.getT();
                    final double velocity;

                    if (useRealScale)
                        velocity = getScaledDistance(d1, d2) / timeInterval;
                    else
                        velocity = getDistance(d1, d2);

                    // set velocity for this time point
                    row.createCell(t + 1, CellType.NUMERIC).setCellValue(velocity);

                    // store max T
                    if (t > maxT)
                        maxT = t;
                }
            }

            trackNumber++;
        }

        // set time point position
        for (int t = 0; t <= maxT; t++)
            header2.createCell(t + 1, CellType.STRING)
                    .setCellValue(MathUtil.roundSignificant(t * timeInterval, 6, true));

        try
        {
            final FileOutputStream out = new FileOutputStream(file);

            // save workbook
            wb.write(out);
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /** @deprecated */
    private void exportVelocitiesToConsole()
    {

        // int cnt = 0;
        // boolean useRealScale = useRealScalesBox.isSelected();
        // for ( TrackSegment ts : trackPool.getTrackSegmentList() )
        // //if ( ts.isAllDetectionEnabled() )
        // {
        // System.out.println("Track "+cnt);
        // cnt++;
        // for ( int i = 0; i < ts.detectionList.size() - 1 ; i++ )
        //
        // {
        // Detection d1 = ts.detectionList.get( i );
        // Detection d2 = ts.detectionList.get( i + 1 );
        //
        // if ( d1.isEnabled() && d2.isEnabled() )
        // {
        //
        // float velocity;
        // float t;
        // if (useRealScale)
        // {
        // velocity = (float) (getScaledDistance( d1 , d2 )/super.trackPool.getSequence().getTScale());
        // t = (float)(((float)d1.getT())*super.trackPool.getSequence().getTScale());
        // }
        // else
        // {
        // velocity = (float) getDistance( d1 , d2 );
        // t = d1.getT();
        // }
        // System.out.println(t+" "+velocity);
        // }
        // }
        // }

        // boolean useRealScale = true; //useRealScalesBox.isSelected();
        double timeInterval = trackPool.getDisplaySequence().getTimeInterval();

        for (TrackSegment ts : trackPool.getTrackSegmentList())
        {
            ArrayList<Detection> detectionList = ts.getDetectionList();
            for (int i = 0; i < detectionList.size() - 1; i++)

            {
                Detection d1 = detectionList.get(i);
                Detection d2 = detectionList.get(i + 1);

                if (d1.isEnabled() && d2.isEnabled())
                {

                    float velocity;
                    float t;
                    if (useRealScale)
                    {
                        velocity = (float) (getScaledDistance(d1, d2) / timeInterval);
                        t = (float) (((float) d1.getT()) * timeInterval);
                    }
                    else
                    {
                        velocity = (float) getDistance(d1, d2);
                        t = d1.getT();
                    }
                    // PluginConsole.println(""+velocity);
                    // System.out.println(t+" "+velocity);
                }
            }
        }
    }

    // @Override
    // public void Close() {
    // trackPool.getSequence().removePainter( this );
    // }
    //
    // public void ROIChanged(SequenceChangeEvent e) {
    // }
    //
    // public void dimensionChanged(SequenceChangeEvent e) {
    // }
    //
    // public void nameChanged(SequenceChangeEvent e) {
    // }
    //
    // public void selectionChanged(SequenceChangeEvent e) {
    // if ( displayTextCheckBox.isSelected() )
    // trackPool.fireTrackEditorProcessorChange();
    //
    // }

    // ------------------------------------------------------------------
    // Painter
    // ------------------------------------------------------------------

    public void keyPressed(Point p, KeyEvent e)
    {
    }

    public void mouseClick(Point p, MouseEvent e)
    {
    }

    public void mouseDrag(Point p, MouseEvent e)
    {
    }

    public void mouseMove(Point p, MouseEvent e)
    {
    }

    Rectangle2D chartRectangleInSequence = new Rectangle2D.Float(250, 20 + 260 * 0, 490, 240);

    /** @deprecated */
    public void analyseTracksToTextFile(ArrayList<TrackSegment> tsList)
    {
        JFileChooser chooser = new JFileChooser();
        chooser.setDialogTitle("Select directory where to save files");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showOpenDialog(null);
        if (returnVal != JFileChooser.APPROVE_OPTION)
            return;
        File directory = chooser.getSelectedFile();

        if (!directory.isDirectory())
        {
            MessageDialog.showDialog("Please select a folder", MessageDialog.ERROR_MESSAGE);
        }

        // first save positions
        File fPositions = new File(directory.getAbsolutePath() + "/positions.txt");
        FileOutputStream outStream;
        try
        {
            outStream = new FileOutputStream(fPositions);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return;
        }
        PrintStream printOut = new PrintStream(outStream);
        int minT = tsList.get(0).getDetectionAt(0).getT();
        int maxT = minT;
        for (TrackSegment ts : tsList)
            for (Detection d : ts.getDetectionList())
                if (d.getT() > maxT)
                    maxT = d.getT();
                else if (d.getT() < minT)
                    minT = d.getT();

        String legend = "time \t";
        for (TrackSegment ts : tsList)
            legend = legend.concat("x \t y \t");
        printOut.println(legend);

        // double xscale = super.trackPool.getSequence().getXScale();
        // double yscale = super.trackPool.getSequence().getYScale();
        // double tscale = super.trackPool.getSequence().getTScale();
        //
        double xscale = 1;
        double yscale = 1;
        double tscale = 1;

        for (int t = minT; t <= maxT; t++)
        {
            String s = Integer.toString(t);
            for (TrackSegment ts : tsList)
            {
                s = s.concat("\t");
                for (Detection d : ts.getDetectionList())
                    if (d.getT() == t)
                    {
                        s = s.concat(Double.toString(d.getX() * xscale) + "\t" + (d.getY() * yscale) + "\t");
                        break;
                    }
            }
            printOut.println(s);
        }
        printOut.close();
        try
        {
            outStream.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        // then save velocities
        File fVelocities = new File(directory.getAbsolutePath() + "/velocities.txt");
        FileOutputStream outStream2;
        try
        {
            outStream2 = new FileOutputStream(fVelocities);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
            return;
        }
        PrintStream printOut2 = new PrintStream(outStream2);
        legend = "time \t";
        for (int a = 0; a < tsList.size(); a++)
        {
            legend = legend.concat("velocity " + a + "\t");
        }
        printOut2.println(legend);

        for (int t = minT; t <= maxT; t++)
        {
            String s = Integer.toString(t);
            for (TrackSegment ts : tsList)
            {
                s = s.concat("\t");
                Detection d1 = null;
                Detection d2 = null;
                for (Detection d : ts.getDetectionList())
                    if (d.getT() == t)
                    {
                        d1 = d;
                        break;
                    }
                if (d1 != null)
                {
                    for (Detection d : ts.getDetectionList())
                        if (d.getT() == t + 1)
                        {
                            d2 = d;
                            break;
                        }

                }
                if (d1 != null && d2 != null)
                {
                    s = s.concat(Double.toString(Math.sqrt((Math.pow((d2.getX() - d1.getX()) * xscale, 2)
                            + Math.pow((d2.getY() - d1.getY()) * yscale, 2))) / tscale));
                }
            }
            printOut2.println(s);
        }
        printOut2.close();
        try
        {
            outStream2.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void displaySequenceChanged()
    {

        for (Sequence sequence : Icy.getMainInterface().getSequencesContaining(this))
        {
            sequence.removePainter(this);
        }

        Sequence sequence = trackPool.getDisplaySequence();
        if (sequence != null)
        {
            sequence.addPainter(this);
        }

    }

    @Override
    public void keyPressed(KeyEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyReleased(KeyEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseClick(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseDrag(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseMove(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void mousePressed(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void mouseReleased(MouseEvent e, Point2D imagePoint, IcyCanvas canvas)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void paint(Graphics2D g, Sequence sequence, IcyCanvas canvas)
    {

        // System.out.println("painting...");

        // remove painter from sequence and remove from internal painterList.
        // {
        // for ( Painter p : painterList )
        // {
        // trackPool.getSequence().removePainter( p );
        // }
        // painterList.clear();
        // }

        // affect built painters to the sequence
        {
            for (Painter p : painterList)
            {
                // trackPool.getSequence().addPainter( p );
                p.paint(g, sequence, canvas);
            }
            // trackPool.getSequence().overlayChangedDelayed();
        }

        if (g == null)
            return;

        Graphics2D g2 = (Graphics2D) g;

        // scaling operation.

        double scale = Double.parseDouble(scaleTextField.getText());
        double minX = chartRectangleInSequence.getCenterX();
        double minY = chartRectangleInSequence.getCenterY();

        Rectangle2D transformedChartRectangleInSequence = (Rectangle2D) chartRectangleInSequence.clone();
        transformedChartRectangleInSequence.setRect((-chartRectangleInSequence.getWidth() / 2) * (1d / scale),
                (-chartRectangleInSequence.getHeight() / 2) * (1d / scale),
                chartRectangleInSequence.getWidth() * (1d / scale),
                chartRectangleInSequence.getHeight() * (1d / scale));

        AffineTransform transform = g2.getTransform();

        g2.scale(scale, scale);
        g2.translate(minX * (1d / scale), minY * (1d / scale));

        if (displayGraphInSequenceCheckBox.isSelected())
        {
            chart.draw((Graphics2D) g, transformedChartRectangleInSequence);
        }

        g2.setTransform(transform);

        // get a reference to the plot for further customisation...

    }

    @Override
    public void Close()
    {
        // TODO Auto-generated method stub

    }
}
